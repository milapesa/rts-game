#include "RTSHUD.h"

#include "RTSGamePlayerController.h"
#include "AI/RTSAICharacter.h"

void ARTSHUD::DrawHUD()
{
	Super::DrawHUD();

	if(bIsSelecting)
	{
		FVector2d CurrentMousePosition = GetMousePosition();
		
		DrawRect(FLinearColor(0, 255, 255, 0.5), InitialMousePosition.X, InitialMousePosition.Y,
			CurrentMousePosition.X - InitialMousePosition.X, CurrentMousePosition.Y - InitialMousePosition.Y);

		TArray<ARTSAICharacter*> SelectedCharacters;
		GetActorsInSelectionRectangle(InitialMousePosition, CurrentMousePosition, SelectedCharacters, false, true);
		const TObjectPtr<ARTSGamePlayerController> PlayerController = Cast<ARTSGamePlayerController>(GetOwningPlayerController());
		PlayerController->SetActorsSelected(SelectedCharacters);
	}
}

void ARTSHUD::StartSelection()
{
	InitialMousePosition = GetMousePosition();
	bIsSelecting = true;
}

void ARTSHUD::StopSelection()
{
	bIsSelecting = false;
}

FVector2d ARTSHUD::GetMousePosition() const
{
	float PositionX;
	float PositionY;
	
	GetOwningPlayerController()->GetMousePosition(PositionX, PositionY);

	return FVector2d(PositionX, PositionY);
}

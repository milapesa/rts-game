#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "RTSPlayerPawn.generated.h"

class UFloatingPawnMovement;
class UCameraComponent;
class USpringArmComponent;

UCLASS()
class RTSGAME_API ARTSPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	ARTSPlayerPawn();

private:	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<USceneComponent> SceneComponent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<USpringArmComponent> SpringArmComponent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UCameraComponent> CameraComponent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta= (AllowPrivateAccess = "true"))
	TObjectPtr<UFloatingPawnMovement> FloatingPawnMovement;
};

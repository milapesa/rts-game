#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "GameFramework/PlayerController.h"

#include "RTSGamePlayerController.generated.h"

struct FEQSParametrizedQueryExecutionRequest;

class ARTSAICharacter;
class ARTSHUD;
class IRTSDamageableInterface;
class UInputAction;
class UInputMappingContext;
class UNiagaraSystem;

struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UCLASS()
class ARTSGamePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ARTSGamePlayerController();

	/** Sets which are actors are currently selected */
	void SetActorsSelected(const TArray<ARTSAICharacter*>& SelectedCharacters);

	/** Returns the last cached mouse position */
	UFUNCTION(BlueprintPure)
	const FVector& GetMouseCachedDestination() const { return CursorCachedPosition; }
	
protected:
	virtual void Tick(float DeltaSeconds) override;

	/** Set-ups the enhanced input component */
	virtual void SetupInputComponent() override;

	/** Moves the pawn to the direction of the input */
	void Move(const FInputActionValue& InputActionValue);

	/** Moves the pawn to the direction of the cursor when on the screen edge */
	void EdgeMovement() const;

	/** Starts the cursor selection */
	void OnSelectStarted();

	/** Stops the cursor selection */
	void OnSelectReleased();

	void MultiSelect(bool bStarted);
	
	/** Gives a command to the selected character */
	void OnSetDestinationTriggered();

	/** Plays a VFX */
	void OnSetDestinationReleased();

	/** Gives the selected characters the move command to the calculated positions */
	void MoveFormationCalculated(TSharedPtr<FEnvQueryResult> EnvQueryResult, TArray<ARTSAICharacter*> InCharactersSelected);

	/** Gives the selected characters the attack command to the calculated positions */
	void AttackFormationCalculated(TSharedPtr<FEnvQueryResult> EnvQueryResult, TArray<ARTSAICharacter*> InCharactersSelected, AActor* Target);

	/** Assigns the best possible location for each character */
	TArray<TTuple<ARTSAICharacter*, FVector>> AssignLocationToCharacters(TArray<FVector>& Locations, TArray<ARTSAICharacter*>& InCharacters) const;

	/** If the selected character dies it removes if from the selection */
	UFUNCTION()
	void OnSelectedCharacterDamaged(TScriptInterface<IRTSDamageableInterface> SelectedCharacter, float PreviousHealth, float NewHealth);

	ARTSHUD* GetCameraHUD() const;

private:
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UInputMappingContext> DefaultMappingContext;
	
	/** Used to give the selected troops a command */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UInputAction> SetDestinationClickAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UInputAction> MoveAction;

	/** Select Troops Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UInputAction> SelectAction;

	/** Multi Select Actors with Ctrl Key Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	TObjectPtr<UInputAction> MultiSelectAction;
	
	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, Category = Input)
	TObjectPtr<UNiagaraSystem> FXCursor = nullptr;

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, Category = Input)
	float ShortPressThreshold = 0.1f;

	/** How far from the screen the cursor needs to be so the pawn moves towards its direction */
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float MoveMouseMargin = 0.0f;

	/** Used to calculate the positions of the selected characters on a move command */
	UPROPERTY(EditAnywhere, Category = Formation)
	FEQSParametrizedQueryExecutionRequest MoveFormationEQS;

	/** Used to calculate the positions of the selected characters on an attack command */
	UPROPERTY(EditAnywhere, Category = Formation)
	FEQSParametrizedQueryExecutionRequest AttackFormationEQS;

	/** Troops currently selected */
	UPROPERTY(Transient)
	TArray<ARTSAICharacter*> CharactersSelected;

	FVector CursorCachedPosition = FVector::ZeroVector;

	/** How the long the set destination action was triggered */
	float FollowTime = 0.0f;

	/** Current formation query being calculated */
	int32 FormationQueryID = INDEX_NONE;

	bool bMultiSelect = false;
};



#pragma once

#include "CoreMinimal.h"
#include "GenericTeamAgentInterface.h"
#include "GameFramework/Character.h"
#include "RTSGame/AnimationNotifies/RTSTraceNotifyInterface.h"
#include "RTSGame/Damageable/RTSDamageableInterface.h"

#include "RTSAICharacter.generated.h"

UCLASS()
class RTSGAME_API ARTSAICharacter : public ACharacter, public IGenericTeamAgentInterface, public IRTSDamageableInterface, public IRTSTraceNotifyInterface
{
	GENERATED_BODY()

public:

	// ~ Begin IGenericTeamAgentInterface

	/** Returns the controller team ID */
	virtual FGenericTeamId GetGenericTeamId() const override;

	virtual void SetGenericTeamId(const FGenericTeamId& TeamID) override;
	
	// ~ End IGenericTeamAgentInterface

	// ~ Begin IRTSTraceNotifyInterface

	/** Damages the hit actor */
	virtual void NotifyHit(const FHitResult& Hit) override;

	// ~ End IRTSTraceNotifyInterface

	// ~ Begin IRTSDamageableInterface

	/** Reduces the health of the character */
	virtual void Damage(float Damage, const FHitResult& Hit) override;

	virtual FRTSOnDamaged& GetOnDamagedDelegate() override { return OnCharacterDamaged; }

	virtual float GetCurrentHealth() const override { return Health; }

	// ~ End IRTSDamageableInterface

	UFUNCTION(BlueprintImplementableEvent)
	void Select();

	UFUNCTION(BlueprintImplementableEvent)
	void UnSelect();

	bool CanBeSelected() const;
	
	FRTSOnDamaged OnCharacterDamaged;

protected:
	virtual void PostInitializeComponents() override;

	void Die();

private:
	/** How much health each AI attack will reduce */
	UPROPERTY(EditAnywhere)
	float DamageOnHit = 10.0f;
	
	UPROPERTY(EditAnywhere)
	float MaxHealth = 100.0f;
	
	float Health = 0.0f;

	bool bDead = false;
};

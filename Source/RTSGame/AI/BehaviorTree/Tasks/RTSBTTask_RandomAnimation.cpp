
#include "RTSBTTask_RandomAnimation.h"

#include "AIController.h"
#include "GameFramework/Character.h"

URTSBTTask_RandomAnimation::URTSBTTask_RandomAnimation()
{
	bCreateNodeInstance = true;
	bNotifyTaskFinished = true;
}

EBTNodeResult::Type URTSBTTask_RandomAnimation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	BTComp = OwnerComp;
	const int32 RandomMontageIndex = FMath::RandRange(0, RandomMontages.Num() - 1);
	ExecutingMontage = RandomMontages[RandomMontageIndex];
	
	UAnimInstance* AnimInstance = GetAnimInstance();
	const float Duration = AnimInstance->Montage_Play(ExecutingMontage);
	AnimInstance->OnMontageEnded.AddDynamic(this, &URTSBTTask_RandomAnimation::OnMontageEnded);

	return Duration > 0.0f ? EBTNodeResult::InProgress : EBTNodeResult::Failed;
}

void URTSBTTask_RandomAnimation::OnTaskFinished(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory,
	EBTNodeResult::Type TaskResult)
{
	if (UAnimInstance* AnimInstance = GetAnimInstance())
	{
		AnimInstance->OnMontageEnded.RemoveDynamic(this, &URTSBTTask_RandomAnimation::OnMontageEnded);
	}
	Super::OnTaskFinished(OwnerComp, NodeMemory, TaskResult);
}

void URTSBTTask_RandomAnimation::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage == ExecutingMontage)
	{
		FinishLatentTask(*BTComp, EBTNodeResult::Succeeded);
	}
}

UAnimInstance* URTSBTTask_RandomAnimation::GetAnimInstance() const
{
	if (ACharacter* AICharacter = BTComp->GetAIOwner()->GetPawn<ACharacter>())
	{
		return AICharacter->GetMesh()->GetAnimInstance();
	}
	return nullptr;
}

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"

#include "RTSBTTask_RandomAnimation.generated.h"

/** Executes a random montage */
UCLASS()
class RTSGAME_API URTSBTTask_RandomAnimation : public UBTTaskNode
{
	GENERATED_BODY()

public:
	URTSBTTask_RandomAnimation();
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void OnTaskFinished(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTNodeResult::Type TaskResult) override;

protected:
	UFUNCTION()
	void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);

	UAnimInstance* GetAnimInstance() const;

private:
	UPROPERTY(EditAnywhere)
	TArray<TObjectPtr<UAnimMontage>> RandomMontages;

	UPROPERTY(Transient)
	TObjectPtr<UBehaviorTreeComponent> BTComp = nullptr;

	UPROPERTY(Transient)
	TObjectPtr<UAnimMontage> ExecutingMontage = nullptr;
};

#include "RTSAICharacter.h"

#include "RTSAIController.h"
#include "Components/CapsuleComponent.h"
#include "Perception/AIPerceptionSystem.h"
#include "Perception/AISense_Sight.h"

namespace RTSAICharacterUtils
{
	const FName RagdollCollisionProfileName = TEXT("Ragdoll");
}

FGenericTeamId ARTSAICharacter::GetGenericTeamId() const
{
	if (const IGenericTeamAgentInterface* TeamInterface = Cast<IGenericTeamAgentInterface>(GetController()))
	{
		return TeamInterface->GetGenericTeamId();
	}
	return FGenericTeamId::NoTeam;
}

void ARTSAICharacter::SetGenericTeamId(const FGenericTeamId& TeamID)
{
	if (IGenericTeamAgentInterface* TeamInterface = Cast<IGenericTeamAgentInterface>(GetController()))
	{
		TeamInterface->SetGenericTeamId(TeamID);
	}
}

void ARTSAICharacter::NotifyHit(const FHitResult& Hit)
{
	IRTSDamageableInterface* Damageable = Cast<IRTSDamageableInterface>(Hit.GetActor());
	const ETeamAttitude::Type Attitude = GetTeamAttitudeTowards(*Hit.GetActor());
	if (Damageable && Attitude == ETeamAttitude::Hostile)
	{
		Damageable->Damage(DamageOnHit, Hit);
	}
}

void ARTSAICharacter::Damage(float Damage, const FHitResult& Hit)
{
	float OldHealth = Health;
	Health -= Damage;
	if (Health <= 0.0f && !bDead)
	{
		Die();
	}
	OnCharacterDamaged.Broadcast(this, OldHealth, Health);
}

bool ARTSAICharacter::CanBeSelected() const
{
	return !bDead && GetGenericTeamId() == static_cast<uint8>(RTSAITeams::Player);
}

void ARTSAICharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	Health = MaxHealth;
}

void ARTSAICharacter::Die()
{
	bDead = true;

	// Ragdoll
	USkeletalMeshComponent* SkelMesh = GetMesh();
	SkelMesh->SetCollisionProfileName(RTSAICharacterUtils::RagdollCollisionProfileName);
	SkelMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SkelMesh->SetAllBodiesSimulatePhysics(true);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	if (AController* CharacterController = GetController())
	{
		CharacterController->Destroy();
	}
	
	UAIPerceptionSystem* PerceptionSystem = UAIPerceptionSystem::GetCurrent(GetWorld());
	PerceptionSystem->UnregisterSource(*this);
}

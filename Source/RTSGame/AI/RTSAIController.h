#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "RTSAITypes.h"
#include "Perception/AIPerceptionTypes.h"

#include "RTSAIController.generated.h"

class UAITask_MoveTo;
class URTSAITask_AttackCommand;

namespace RTSAIBlackboardKeyNames
{
	const FName PositionToMove = TEXT("PositionToMove");
	const FName TargetActor = TEXT("TargetActor");
}

UCLASS()
class RTSGAME_API ARTSAIController : public AAIController
{
	GENERATED_BODY()

public:
	ARTSAIController();
	
	UFUNCTION(BlueprintCallable)
	void MoveCommand(const FVector& Location);

	UFUNCTION(BlueprintCallable)
	void AttackCommand(AActor* Target, const FVector& PositionToMove);

protected:
	UFUNCTION()
	void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);

	virtual void OnPossess(APawn* InPawn) override;
	
	virtual void OnUnPossess() override;

private:
	/** Behavior that will be executed when the controller posses a Pawn */
	UPROPERTY(EditAnywhere)
	TObjectPtr<UBehaviorTree> BehaviorTree = nullptr;

	UPROPERTY(EditAnywhere)
	RTSAITeams Team;

	/** Move task given by the player if any */
	UPROPERTY(Transient)
	TObjectPtr<UAITask_MoveTo> CurrentMoveCommand = nullptr;

	/** Attack task given by the player if any */
	UPROPERTY(Transient)
	TObjectPtr<URTSAITask_AttackCommand> CurrentAttackCommand = nullptr;

	/** The actors perceived that need to be attacked */
	UPROPERTY(Transient)
	TMap<TObjectPtr<AActor>, TObjectPtr<URTSAITask_AttackCommand>> PerceptionAttackCommands;
};

#include "RTSAITask_AttackCommand.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "RTSGame/AI/RTSAIController.h"
#include "RTSGame/Damageable/RTSDamageableInterface.h"

URTSAITask_AttackCommand::URTSAITask_AttackCommand(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AddRequiredResource(URTSAIResource_Command::StaticClass());
	AddClaimedResource(URTSAIResource_Command::StaticClass());
	
	bIsPausable = false;
	ResourceOverlapPolicy = ETaskResourceOverlapPolicy::RequestCancelAndStartAtEnd;
}

URTSAITask_AttackCommand* URTSAITask_AttackCommand::Attack(AAIController& AIController, AActor* Target,
                                                           const FVector& PositionToMove)
{
	URTSAITask_AttackCommand* Task = NewAITask<URTSAITask_AttackCommand>(AIController, TEXT("Attack Command"));
	Task->Priority = static_cast<uint8>(EAITaskPriority::High);
	Task->Target = Target;
	Task->PositionToMove = PositionToMove;
	return Task;
}

URTSAITask_AttackCommand* URTSAITask_AttackCommand::PerceptionAttack(AAIController& AIController, AActor* Target)
{
	URTSAITask_AttackCommand* Task = NewAITask<URTSAITask_AttackCommand>(AIController, TEXT("Perception Attack"));
	Task->Priority = static_cast<uint8>(EAITaskPriority::Low);
	Task->Target = Target;
	Task->bIsPausable = true;
	Task->ResourceOverlapPolicy = ETaskResourceOverlapPolicy::StartAtEnd;
	return Task;
}

FString URTSAITask_AttackCommand::GetDebugString() const
{
	return Super::GetDebugString() + FString::Printf(TEXT(" - Attacking: %s"), Target.IsValid() ? *Target->GetActorNameOrLabel() : TEXT("None"));
}

void URTSAITask_AttackCommand::Activate()
{
	Super::Activate();

	if (!Target.IsValid() || Target == GetAIController()->GetPawn())
	{
		EndTask();
		return;
	}
	
	if (IRTSDamageableInterface* DamageableInterface = Cast<IRTSDamageableInterface>(Target))
	{
		if (DamageableInterface->GetCurrentHealth() <= 0.0f)
		{
			EndTask();
			return;
		}
		DamageableInterface->GetOnDamagedDelegate().AddDynamic(this ,&URTSAITask_AttackCommand::OnTargetDamaged);
	}

	SetBlackboardKeyValues();
}

void URTSAITask_AttackCommand::OnDestroy(bool bInOwnerFinished)
{
	ClearBlackboardKeyValues();

	if (IRTSDamageableInterface* DamageableInterface = Cast<IRTSDamageableInterface>(Target))
	{
		DamageableInterface->GetOnDamagedDelegate().RemoveDynamic(this ,&URTSAITask_AttackCommand::OnTargetDamaged);
	}

	Super::OnDestroy(bInOwnerFinished);
}

void URTSAITask_AttackCommand::OnTargetDamaged(TScriptInterface<IRTSDamageableInterface> Damageable, float OldHealth, float NewHealth)
{
	if (NewHealth <= 0.0f)
	{
		// Might be called when the task is paused. If so, we want to call cancel and not end task
		if (IsActive())
		{
			EndTask();
		}
		else
		{
			ExternalCancel();
		}
	}
}

void URTSAITask_AttackCommand::Resume()
{
	Super::Resume();
	SetBlackboardKeyValues();
}

void URTSAITask_AttackCommand::Pause()
{
	ClearBlackboardKeyValues();

	// TODO bIsPausable is not working correctly so I will be overriding the pause function
	if (bIsPausable)
	{
		Super::Pause();
	}
	else
	{
		EndTask();
	}
}

void URTSAITask_AttackCommand::SetBlackboardKeyValues()
{
	UBlackboardComponent* BBComp = GetAIController()->GetBlackboardComponent();
	if (!PositionToMove.Equals(FVector::ZeroVector))
	{
		BBComp->SetValueAsVector(RTSAIBlackboardKeyNames::PositionToMove, PositionToMove);
	}
	BBComp->SetValueAsObject(RTSAIBlackboardKeyNames::TargetActor, Target.Get());
	bKeysSet = true;
}
 
void URTSAITask_AttackCommand::ClearBlackboardKeyValues()
{
	AAIController* AIController = GetAIController();
	UBlackboardComponent* BBComp = AIController ? AIController->GetBlackboardComponent() : nullptr;
	if (BBComp && bKeysSet)
	{
		BBComp->ClearValue(RTSAIBlackboardKeyNames::TargetActor);
		BBComp->ClearValue(RTSAIBlackboardKeyNames::PositionToMove);
	}
}


#pragma once

#include "CoreMinimal.h"
#include "GameplayTaskResource.h"
#include "RTSGame/Damageable/RTSDamageableInterface.h"
#include "Tasks/AITask.h"

#include "RTSAITask_AttackCommand.generated.h"

/** Resource used in AI commands given by the player */
UCLASS()
class URTSAIResource_Command : public UGameplayTaskResource
{
	GENERATED_BODY()
};

/** Gives the AI a target to attack */
UCLASS()
class RTSGAME_API URTSAITask_AttackCommand : public UAITask
{
	GENERATED_BODY()

public:
	URTSAITask_AttackCommand(const FObjectInitializer& ObjectInitializer);

	/** Moves the AI to the given position and then attacks the Target */
	static URTSAITask_AttackCommand* Attack(AAIController& AIController, AActor* Target, const FVector& PositionToMove);

	/** Attacks the target with a low priority. Other commands will pause this task */
	static URTSAITask_AttackCommand* PerceptionAttack(AAIController& AIController, AActor* Target);

	virtual FString GetDebugString() const override;

protected:

	/** Sets the AI target and binds OnTargetDamaged */
	virtual void Activate() override;

	/** Unsets the AI target and unbinds OnTargetDamaged */
	virtual void OnDestroy(bool bInOwnerFinished) override;

	/** Called when the target receives damage. If the target dies, it ends the task. */
	UFUNCTION()
	void OnTargetDamaged(TScriptInterface<IRTSDamageableInterface> Damageable, float OldHealth, float NewHealth);

	/** Sets again the AI target */
	virtual void Resume() override;

	/** Unsets the AI target */
	virtual void Pause() override;

	/** Sets the target on the AI blackboard and the position to move if necessary */
	void SetBlackboardKeyValues();

	/** Clears the target and the position to move from the AI blackboard */
	void ClearBlackboardKeyValues();

private:
	/** The actor the AI will attack */
	TWeakObjectPtr<AActor> Target = nullptr;

	/** The position where the AI will move before attacking the Target */
	FVector PositionToMove = FVector::ZeroVector;

	/** Used to know if we should clear the blackboard keys when the task is paused or ended */
	bool bKeysSet = false;
};

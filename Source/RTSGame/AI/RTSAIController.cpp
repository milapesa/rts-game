#include "RTSAIController.h"

#include "AITasks/RTSAITask_AttackCommand.h"
#include "Perception/AIPerceptionComponent.h"
#include "RTSAITypes.h"
#include "Tasks/AITask_MoveTo.h"

namespace RTSAIControllerUtils
{
	void CancelTask(UGameplayTask* Task)
	{
		if (Task && !Task->IsFinished())
		{
			Task->ExternalCancel();
		}
	}
}

ARTSAIController::ARTSAIController()
{
	Team = RTSAITeams::Player;
}

void ARTSAIController::MoveCommand(const FVector& Location)
{
	RTSAIControllerUtils::CancelTask(CurrentMoveCommand);
	CurrentMoveCommand = UAITask_MoveTo::AIMoveTo(this, Location);
	CurrentMoveCommand->AddClaimedResource(URTSAIResource_Command::StaticClass());
	CurrentMoveCommand->AddRequiredResource(URTSAIResource_Command::StaticClass());
	CurrentMoveCommand->ReadyForActivation();
}

void ARTSAIController::AttackCommand(AActor* Target, const FVector& PositionToMove)
{
	RTSAIControllerUtils::CancelTask(CurrentAttackCommand);
	CurrentAttackCommand = URTSAITask_AttackCommand::Attack(*this, Target, PositionToMove);
	CurrentAttackCommand->ReadyForActivation();
}

void ARTSAIController::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	URTSAITask_AttackCommand* AttackCommand = nullptr;
	if (TObjectPtr<URTSAITask_AttackCommand>* AttackCommandPtr = PerceptionAttackCommands.Find(Actor))
	{
		AttackCommand = *AttackCommandPtr;
	}
	const bool bAttackingActor = AttackCommand && !AttackCommand->IsFinished();

	if (Stimulus.WasSuccessfullySensed())
	{
		if (!bAttackingActor)
		{
			RTSAIControllerUtils::CancelTask(AttackCommand);
			URTSAITask_AttackCommand* NewAttack = URTSAITask_AttackCommand::PerceptionAttack(*this, Actor);
			PerceptionAttackCommands.Add(Actor,NewAttack);
			NewAttack->ReadyForActivation();
		}
	}
	else if (bAttackingActor)
	{
		AttackCommand->ExternalCancel();
		PerceptionAttackCommands.Remove(Actor);
	}
}

void ARTSAIController::OnPossess(APawn* InPawn)
{
	SetGenericTeamId(static_cast<uint8>(Team));
	
	Super::OnPossess(InPawn);

	RunBehaviorTree(BehaviorTree);

	PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &ARTSAIController::OnTargetPerceptionUpdated);
}

void ARTSAIController::OnUnPossess()
{
	PerceptionComponent->OnTargetPerceptionUpdated.RemoveDynamic(this, &ARTSAIController::OnTargetPerceptionUpdated);
	
	Super::OnUnPossess();
}

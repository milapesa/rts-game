#pragma once

#include "CoreMinimal.h"
#include "GenericTeamAgentInterface.h"
#include "RTSAITypes.h"
#include "GameFramework/Actor.h"

#include "RTSAITarget.generated.h"

/** Base actor used for targeting */
UCLASS()
class RTSGAME_API ARTSAITarget : public AActor, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	virtual FGenericTeamId GetGenericTeamId() const override { return static_cast<uint8>(Team); }

	virtual void SetGenericTeamId(const FGenericTeamId& TeamID) override  { Team = static_cast<RTSAITeams>(TeamID.GetId()); }

private:
	UPROPERTY(EditAnywhere)
	RTSAITeams Team;
};

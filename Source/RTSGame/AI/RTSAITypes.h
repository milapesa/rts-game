#pragma once

#include "CoreMinimal.h"

UENUM()
enum class RTSAITeams : uint8
{
	Player = 0,
	Target = 1,
};
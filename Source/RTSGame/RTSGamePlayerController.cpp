#include "RTSGamePlayerController.h"

#include "AI/RTSAICharacter.h"
#include "AI/RTSAIController.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Engine/LocalPlayer.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "GameFramework/Pawn.h"
#include "InputActionValue.h"
#include "NiagaraFunctionLibrary.h"
#include "RTSHUD.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

ARTSGamePlayerController::ARTSGamePlayerController()
{
	//Initialize variables
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CursorCachedPosition = FVector::ZeroVector;
	FollowTime = 0.f;
	MoveMouseMargin = 0.05f;
}


void ARTSGamePlayerController::SetActorsSelected(const TArray<ARTSAICharacter*>& SelectedCharacters)
{
	TArray<ARTSAICharacter*> NewCharacters = SelectedCharacters.FilterByPredicate([](const ARTSAICharacter* SelectedCharacter) { return SelectedCharacter->CanBeSelected(); });

	for (ARTSAICharacter* SelectedCharacter : CharactersSelected)
	{
		SelectedCharacter->OnCharacterDamaged.RemoveDynamic(this, &ARTSGamePlayerController::OnSelectedCharacterDamaged);
		SelectedCharacter->UnSelect();
	}
	
	if (!bMultiSelect)
	{
		CharactersSelected = NewCharacters;
	}
	else
	{
		for (int32 i = NewCharacters.Num() - 1; i >= 0; --i)
		{
			CharactersSelected.AddUnique(NewCharacters[i]);
		}
	}
	
	for (ARTSAICharacter* SelectedCharacter : CharactersSelected)
	{
		SelectedCharacter->OnCharacterDamaged.AddDynamic(this, &ARTSGamePlayerController::OnSelectedCharacterDamaged);
		SelectedCharacter->Select();
	}
}

void ARTSGamePlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	EdgeMovement();
}

void ARTSGamePlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &ARTSGamePlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &ARTSGamePlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &ARTSGamePlayerController::OnSetDestinationReleased);

		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ARTSGamePlayerController::Move);
		EnhancedInputComponent->BindAction(SelectAction, ETriggerEvent::Started, this, &ARTSGamePlayerController::OnSelectStarted);
		EnhancedInputComponent->BindAction(SelectAction, ETriggerEvent::Completed, this, &ARTSGamePlayerController::OnSelectReleased);
		EnhancedInputComponent->BindAction(MultiSelectAction, ETriggerEvent::Started, this, &ARTSGamePlayerController::MultiSelect, true);
		EnhancedInputComponent->BindAction(MultiSelectAction, ETriggerEvent::Completed, this, &ARTSGamePlayerController::MultiSelect, false);
		EnhancedInputComponent->BindAction(MultiSelectAction, ETriggerEvent::Canceled, this, &ARTSGamePlayerController::MultiSelect, false);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void ARTSGamePlayerController::Move(const FInputActionValue& InputActionValue)
{
	APawn* PlayerPawn = GetPawn();
	if (!PlayerPawn)
	{
		return;
	}

	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector ForwardVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	const FVector RightVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	const FVector2d Movement = InputActionValue.Get<FVector2d>();
		
	PlayerPawn->AddMovementInput(ForwardVector, Movement.Y);
	PlayerPawn->AddMovementInput(RightVector, Movement.X);
}

void ARTSGamePlayerController::EdgeMovement() const
{
	UWorld* WorldContext = GetWorld();
	APawn* FloatingPawn = GetPawn();

	if (!WorldContext || !FloatingPawn)
	{
		return;
	}

	FVector2d MousePosition = UWidgetLayoutLibrary::GetMousePositionOnViewport(WorldContext);
	const FVector2d ViewportSize = UWidgetLayoutLibrary::GetViewportSize(WorldContext);

	MousePosition = MousePosition * UWidgetLayoutLibrary::GetViewportScale(WorldContext);
	MousePosition.X = MousePosition.X / ViewportSize.X;
	MousePosition.Y = MousePosition.Y / ViewportSize.Y;

	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector ForwardVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	const FVector RightVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

	const float InvertedMouseMargin = 1.0f - MoveMouseMargin;
	
	if(MousePosition.X > InvertedMouseMargin && MousePosition.X < 1.0f)
	{
		FloatingPawn->AddMovementInput(RightVector, MousePosition.X);
	}
	else if(MousePosition.X < MoveMouseMargin && MousePosition.X > 0.0f)
	{
		FloatingPawn->AddMovementInput(RightVector * -1.0f, 1.0f - MousePosition.X);
	}

	if (MousePosition.Y > InvertedMouseMargin && MousePosition.Y < 1.0f)
	{
		FloatingPawn->AddMovementInput(ForwardVector * -1.0f, MousePosition.Y);
	}
	else if (MousePosition.Y < MoveMouseMargin && MousePosition.Y > 0.0f)
	{
		FloatingPawn->AddMovementInput(ForwardVector,1.0f - MousePosition.Y);
	}
}

void ARTSGamePlayerController::OnSelectStarted()
{
	if (ARTSHUD* CameraHUD = GetCameraHUD())
	{
		CameraHUD->StartSelection();
	}
}

void ARTSGamePlayerController::OnSelectReleased()
{
	if (ARTSHUD* CameraHUD = GetCameraHUD())
	{
		CameraHUD->StopSelection();
	}
}

void ARTSGamePlayerController::MultiSelect(bool bStarted)
{
	bMultiSelect = bStarted;
}

void ARTSGamePlayerController::OnSetDestinationTriggered()
{
	if (CharactersSelected.IsEmpty())
	{
		return;
	}
	
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	FHitResult Hit;
	if (!GetHitResultUnderCursor(ECollisionChannel::ECC_WorldStatic, true, Hit))
	{
		return;
	}
	
	CursorCachedPosition = Hit.Location;

	if (FormationQueryID != INDEX_NONE)
	{
		UEnvQueryManager* QueryManager = UEnvQueryManager::GetCurrent(GetWorld());
		QueryManager->AbortQuery(FormationQueryID);
	}

	const bool bIsDamageable = Cast<IRTSDamageableInterface>(Hit.GetActor()) != nullptr;
	IGenericTeamAgentInterface* TeamAgentInterface = Cast<IGenericTeamAgentInterface>(Hit.GetActor());
	const bool bIsFriendly = TeamAgentInterface ? TeamAgentInterface->GetGenericTeamId() == static_cast<uint8>(RTSAITeams::Player) : false;
	if (bIsDamageable && !bIsFriendly)
	{
		FQueryFinishedSignature Delegate = FQueryFinishedSignature::CreateUObject(this, &ARTSGamePlayerController::AttackFormationCalculated, CharactersSelected, Hit.GetActor());
		FormationQueryID = AttackFormationEQS.Execute(*this, nullptr, Delegate);
		return;
	}


	FQueryFinishedSignature Delegate = FQueryFinishedSignature::CreateUObject(this, &ARTSGamePlayerController::MoveFormationCalculated, CharactersSelected);
	FormationQueryID = MoveFormationEQS.Execute(*this, nullptr, Delegate);
}

void ARTSGamePlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold && CharactersSelected.Num() > 0)
	{
		// We move there and spawn some particles
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CursorCachedPosition, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.f;
}

void ARTSGamePlayerController::MoveFormationCalculated(TSharedPtr<FEnvQueryResult> EnvQueryResult, TArray<ARTSAICharacter*> InCharactersSelected)
{
	FormationQueryID = INDEX_NONE;
	
	if (!EnvQueryResult->IsSuccessful())
	{
		return;
	}
	TArray<FVector> Locations;
	EnvQueryResult->GetAllAsLocations(Locations);
	TArray<TTuple<ARTSAICharacter*, FVector>> CharactersWithLocations = AssignLocationToCharacters(Locations, InCharactersSelected);
	for (const TTuple<ARTSAICharacter*, FVector>& CharacterWithLocation : CharactersWithLocations)
	{
		if (ARTSAIController* Controller = CharacterWithLocation.Key->GetController<ARTSAIController>())
		{
			Controller->MoveCommand(CharacterWithLocation.Value);
		}
	}
}

void ARTSGamePlayerController::AttackFormationCalculated(TSharedPtr<FEnvQueryResult> EnvQueryResult,
	TArray<ARTSAICharacter*> InCharactersSelected, AActor* Target)
{
	FormationQueryID = INDEX_NONE;
	
	if (!EnvQueryResult->IsSuccessful())
	{
		return;
	}
	TArray<FVector> Locations;
	EnvQueryResult->GetAllAsLocations(Locations);
	TArray<TTuple<ARTSAICharacter*, FVector>> CharactersWithLocations = AssignLocationToCharacters(Locations, InCharactersSelected);
	for (const TTuple<ARTSAICharacter*, FVector>& CharacterWithLocation : CharactersWithLocations)
	{
		if (ARTSAIController* Controller = CharacterWithLocation.Get<0>()->GetController<ARTSAIController>())
		{
			Controller->AttackCommand(Target, CharacterWithLocation.Get<1>());
		}
	}
}

TArray<TTuple<ARTSAICharacter*, FVector>> ARTSGamePlayerController::AssignLocationToCharacters(TArray<FVector>& Locations,
	TArray<ARTSAICharacter*>& InCharacters) const
{
	TArray<TTuple<ARTSAICharacter*, FVector>> Result;

	if (Locations.Num() < InCharacters.Num())
	{
		// There are not enough locations for all the characters
		return Result;
	}

	// Remove unnecessary locations. Locations are sorted from best to worst
	Locations.RemoveAt(InCharacters.Num(), Locations.Num() - InCharacters.Num());
	

	// Sort the characters by their distance with the formation center
	FVector Center = Locations[0];
	InCharacters.Sort([Center](const ARTSAICharacter& A, const ARTSAICharacter& B)
	{
		return FVector::DistSquared(Center, A.GetActorLocation()) < FVector::DistSquared(Center, B.GetActorLocation()); 
	});

	// Assign the characters the furthest point possible, so it doesn't block other characters when reaching its destination
	for (ARTSAICharacter* CharacterSelected : InCharacters)
	{
		int32 FurthestLocation = INDEX_NONE;
		float FurthestDistanceSqr = -1.0f;
		for (int32 i = 0; i < Locations.Num(); ++i)
		{
			double Distance = FVector::DistSquared(Locations[i], CharacterSelected->GetActorLocation());
			if (Distance > FurthestDistanceSqr)
			{
				FurthestDistanceSqr = Distance;
				FurthestLocation = i;
			}
		}
		if (!Locations.IsValidIndex(FurthestLocation))
		{
			continue;
		}

		Result.Add(MakeTuple(CharacterSelected, Locations[FurthestLocation]));

		Locations.RemoveAtSwap(FurthestLocation);
	}

	return Result;
}

void ARTSGamePlayerController::OnSelectedCharacterDamaged(TScriptInterface<IRTSDamageableInterface> SelectedCharacter, float PreviousHealth, float NewHealth)
{
	if (NewHealth <= 0.0f)
	{
		ARTSAICharacter* DeadCharacter = Cast<ARTSAICharacter>(SelectedCharacter.GetObject());
		CharactersSelected.Remove(DeadCharacter);
		DeadCharacter->UnSelect();
	}
}

ARTSHUD* ARTSGamePlayerController::GetCameraHUD() const
{
	return Cast<ARTSHUD>(GetHUD());
}


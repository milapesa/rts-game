#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RTSDamageableInterface.generated.h"

UINTERFACE()
class URTSDamageableInterface : public UInterface
{
	GENERATED_BODY()
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FRTSOnDamaged, TScriptInterface<IRTSDamageableInterface>, Damageable, float, OldHealth, float, NewHealth);

/** Gives the functionality to receive damage  */
class RTSGAME_API IRTSDamageableInterface
{
	GENERATED_BODY()

public:
	virtual void Damage(float Damage, const FHitResult& Hit) = 0;

	virtual float GetCurrentHealth() const = 0;

	virtual FRTSOnDamaged& GetOnDamagedDelegate() = 0;
};

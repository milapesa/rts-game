#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "RTSHUD.generated.h"

class ARTSAICharacter;

UCLASS()
class RTSGAME_API ARTSHUD : public AHUD
{
	GENERATED_BODY()

public:

	/** Draws a box representing the player troops selection */
	virtual void DrawHUD() override;

	void StartSelection();

	void StopSelection();

protected:
	
	FVector2d GetMousePosition() const;

private:
	bool bIsSelecting = false;

	/** Mouse position when the selection started */
	FVector2D InitialMousePosition = FVector2D::ZeroVector;
};

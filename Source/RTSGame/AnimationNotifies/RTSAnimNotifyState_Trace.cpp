#include "RTSAnimNotifyState_Trace.h"

#include "RTSTraceNotifyInterface.h"

namespace RTSAnimNotifyStateTrace
{
#if !UE_BUILD_SHIPPING
	static TAutoConsoleVariable<bool> CVarShowDebugTraces(
	TEXT("RTS.Trace.Debug"),
	false,
	TEXT("Show the traces made by the anim notify"),
	ECVF_Default|ECVF_Cheat);
#endif // !UE_BUILD_SHIPPING
}

void URTSAnimNotifyState_Trace::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	float FrameDeltaTime, const FAnimNotifyEventReference& EventReference)
{
	Super::NotifyTick(MeshComp, Animation, FrameDeltaTime, EventReference);

	AActor* OwnerActor = MeshComp->GetOwner();
	IRTSTraceNotifyInterface* TraceNotify = Cast<IRTSTraceNotifyInterface>(OwnerActor);
	if (!TraceNotify)
	{
		return;
	}

	TArray<FHitResult> HitResults;
	FVector BoneLocation = MeshComp->GetBoneLocation(Bone, EBoneSpaces::WorldSpace);
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(OwnerActor);
	FCollisionShape Sphere = FCollisionShape::MakeSphere(TraceRadius);
	UWorld* World = MeshComp->GetWorld();
	const bool bHit = World->SweepMultiByChannel(HitResults, BoneLocation, BoneLocation,
		FQuat::Identity, TraceCollisionChannel, Sphere, Params);

#if !UE_BUILD_SHIPPING
	if (RTSAnimNotifyStateTrace::CVarShowDebugTraces.GetValueOnGameThread())
	{
		FColor DebugColor = bHit ? FColor::Red : FColor::Green;
		DrawDebugSphere(World, BoneLocation, TraceRadius, 20, DebugColor);
	}
#endif // !UE_BUILD_SHIPPING
	
	if (!bHit)
	{
		return;
	}

	for (const FHitResult& HitResult : HitResults)
	{
		if (ActorsHit.Contains(HitResult.GetActor()))
		{
			continue;;
		}
		ActorsHit.Add(HitResult.GetActor());
		TraceNotify->NotifyHit(HitResult);
	}
}

void URTSAnimNotifyState_Trace::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	const FAnimNotifyEventReference& EventReference)
{
	ActorsHit.Reset();
	
	Super::NotifyEnd(MeshComp, Animation, EventReference);
}

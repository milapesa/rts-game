#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "RTSAnimNotifyState_Trace.generated.h"

/** Makes a sphere traces around the given Bone and notifies the owner actor if something hits.
 * The owner needs to implement IRTSTraceNotifyInterface. */
UCLASS()
class RTSGAME_API URTSAnimNotifyState_Trace : public UAnimNotifyState
{
	GENERATED_BODY()

public:

	/** Checks if the bone hits an actor while the animation is executing */
	virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime, const FAnimNotifyEventReference& EventReference) override;

	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;

private:
	UPROPERTY(EditAnywhere)
	FName Bone;

	UPROPERTY(EditAnywhere)
	TEnumAsByte<ECollisionChannel> TraceCollisionChannel = ECC_Visibility;

	/** The radius of the sphere trace */
	UPROPERTY(EditAnywhere)
	float TraceRadius = 30.0f;

	/** Used to hit an actor only once */
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>> ActorsHit;
};

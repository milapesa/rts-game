#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RTSTraceNotifyInterface.generated.h"

UINTERFACE()
class URTSTraceNotifyInterface : public UInterface
{
	GENERATED_BODY()
};

/** Used with the URTSAnimNotifyState_Trace to notify the anim notify state owner a hit */
class RTSGAME_API IRTSTraceNotifyInterface
{
	GENERATED_BODY()

public:
	virtual void NotifyHit(const FHitResult& Hit) = 0;
};

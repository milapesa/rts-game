#include "RTSGameGameMode.h"
#include "RTSGamePlayerController.h"
#include "UObject/ConstructorHelpers.h"

ARTSGameGameMode::ARTSGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARTSGamePlayerController::StaticClass();

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}